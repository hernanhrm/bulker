package main

import (
	"EDteam/bulker/repository"
	"EDteam/cursos/configuration"
	"EDteam/cursos/logger"
	"EDteam/cursos/models/image"
	"EDteam/edimage"
	"EDteam/filemanager"
	"EDteam/psql"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

const token = "?authorization=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoyNzA2LCJlbWFpbCI6Imhlcm5hbnJleWVzNzMyOUBvdXRsb29rLmNvbSIsInJvbGVfaWQiOjEsInNlc3Npb25faWQiOjczODUyOCwiaXBfY2xpZW50IjoiMTkwLjI0Mi4yNS4yNyIsInVzZXJfdHlwZSI6MSwiZXhwIjoxNTg2Mjc1NzUyLCJpc3MiOiJDdXJzb3MgRUR0ZWFtIn0.ZlpTBIZtL6GArMxc7IOxjiqL4iWIh012bDEPoiuezJdXDmB9KrstpG0v9AotPxeCHSZ0ZOgTymek3vlBBnFhefsKmfWZ5hg_k0_7C2qNoVSS7jdOFOS04xo_EKRY3WLHBkpbMPHtPKmTh-6U_KaP4Bi6uM6fyMpvTW3iVW_K6oQ"

func main() {
	cnf := configuration.GetInstance()
	logger.SetFileDestination(cnf.LogFolder)

	err := psql.New(cnf.DBUser, cnf.DBPassword, cnf.DBServer, cnf.DBDatabase, cnf.DBPort)
	if err != nil {
		logger.Error.Printf("no se pudo obtener una conexion a la base de datos: %v", err)
		os.Exit(1)
	}

	crearDirectorioSiNoExiste("./sqlfiles")
	crearDirectorioSiNoExiste("./sqlfiles/before")
	crearDirectorioSiNoExiste("./sqlfiles/faild")
	crearDirectorioSiNoExiste("./sqlfiles/now")

	processAll()
	// processAllArrays()
}

func crearDirectorioSiNoExiste(directorio string) {
	if _, err := os.Stat(directorio); os.IsNotExist(err) {
		err = os.Mkdir(directorio, 0755)
		if err != nil {
			// Aquí puedes manejar mejor el error, es un ejemplo
			log.Fatal(err)
		}
	}
}

func processAll() {
	db := psql.GetConnection()
	config := configuration.GetInstance()

	repo := image.GetStorage(config.DBEngine, db)
	newService := image.NewService(repo)

	storage, err := filemanager.NewService(filemanager.AWS)
	if err != nil {
		log.Fatal(err)
	}

	for key, query := range repository.Directories {

		images, err := repository.GetAll(query.Get)
		if err != nil {
			log.Fatal(err)
		}

		if len(images) == 0 {
			continue
		}

		file, err := os.Create("sqlfiles/now/" + key + "-now.sql")
		if err != nil {
			log.Fatal(err)
		}

		fileBefore, err := os.Create("sqlfiles/before/" + key + "-before.sql")
		if err != nil {
			log.Fatal(err)
		}

		fileFaild, err := os.Create("sqlfiles/faild/" + key + "-faild.sql")
		if err != nil {
			log.Fatal(err)
		}

		bf := bytes.Buffer{}
		fmt.Println("==================================================================")

		for k, img := range images {
			url := img.Picture

			if key == "certificates" ||
				key == "speciality-certificates" ||
				key == "users" {
				url = fmt.Sprintf("%s%s", query.Url, img.Picture)
			}

			if key == "supports" {
				url = fmt.Sprintf("%s%s%s", query.Url, img.Picture, token)
			}

			res, err := http.Get(url)
			if err != nil {
				bf.WriteString(strconv.Itoa(int(img.ID)))
				bf.WriteString(", ")
				logger.Trace.Printf("carpeta: %s : http.Get(url): error %v", key, err)

				continue
			}

			bff := &bytes.Buffer{}

			_, err = io.Copy(bff, res.Body)
			if err != nil {
				bf.WriteString(strconv.Itoa(int(img.ID)))
				bf.WriteString(", ")
				logger.Trace.Printf("carpeta: %s io.Copy(): error %v", key, err)

				continue
			}

			dst := key

			fmt.Printf("%s: %d/%d\n", dst, len(images), k+1)

			director := newService.GetDirector(filepath.Ext(img.Picture), false)
			i, err := edimage.DecodeImage(bff)
			if err != nil {
				bf.WriteString(strconv.Itoa(int(img.ID)))
				bf.WriteString(", ")

				logger.Trace.Printf("carpeta: %s: newService.GetDirector(): error %v", key, err)

				continue
			}

			filename := fmt.Sprintf("%s%s", edimage.GenerateFilename(), filepath.Ext(img.Picture))

			if key == "users" {
				variants := strings.Split("avatar,thumbnail", ",")
				for _, variant := range variants {
					variant = strings.TrimSpace(variant)

					variantImg, err := director.BuildVariant(i, edimage.VariantName(variant))
					if err != nil {
						bf.WriteString(strconv.Itoa(int(img.ID)))
						bf.WriteString(", ")
						logger.Trace.Printf("carpeta: %s: director.BuildVariant(): error %v", key, err)
						continue
					}
					path := fmt.Sprintf("%s/%v/%s", key, variant, filename)

					err = storage.Upload(variantImg, path, filemanager.PublicRead)
					if err != nil {
						bf.WriteString(strconv.Itoa(int(img.ID)))
						bf.WriteString(", ")
						logger.Trace.Printf("carpeta: %s: storage.Upload(): error %v", key, err)

						continue
					}
				}
			} else {
				path := fmt.Sprintf("%s/%s/%s", key, string(edimage.Original), filename)

				originalImg, err := director.Build(i, 100)
				if err != nil {
					bf.WriteString(strconv.Itoa(int(img.ID)))
					bf.WriteString(", ")
					logger.Trace.Printf("carpeta: %s: director.Build: error %v", key, err)

					continue
				}

				if key == "supports" {
					err = storage.Upload(originalImg, path, filemanager.AuthenticatedRead)
					if err != nil {
						bf.WriteString(strconv.Itoa(int(img.ID)))
						bf.WriteString(", ")
						logger.Trace.Printf("carpeta: %s: storage.Upload(originalImg, path, filemanager.AuthenticatedRead): error %v", key, err)

						continue
					}
				} else {
					err = storage.Upload(originalImg, path, filemanager.PublicRead)
					if err != nil {
						bf.WriteString(strconv.Itoa(int(img.ID)))
						bf.WriteString(", ")
						logger.Trace.Printf("carpeta: %s: storage.Upload(originalImg, path, filemanager.AuthenticatedRead): error %v", key, err)

						continue
					}
				}
			}

			res.Body.Close()

			queryGood := fmt.Sprintf(query.UpdateAlt, filename, img.ID)
			file.Write([]byte(queryGood))

			queryBefore := fmt.Sprintf(query.UpdateAlt, img.Picture, img.ID)
			fileBefore.Write([]byte(queryBefore))

		}

		if bf.Len() > 0 {
			bf.Truncate(bf.Len() - 2)
			queryFaild := fmt.Sprintf(query.Faild, bf.String())
			fileFaild.Write([]byte(queryFaild))
		}

		file.Close()
		fileBefore.Close()
		fileFaild.Close()
	}
}

func processAllArrays() {
	db := psql.GetConnection()
	config := configuration.GetInstance()

	repo := image.GetStorage(config.DBEngine, db)
	newService := image.NewService(repo)

	storage, err := filemanager.NewService(filemanager.AWS)
	if err != nil {
		log.Fatal(err)
	}

	for key, query := range repository.ArrayDirectories {
		data, err := repository.GetAllArrays(query.Get)

		file, err := os.Create("sqlfiles/now/" + key + "-now.sql")
		if err != nil {
			log.Fatal(err)
		}

		fileBefore, err := os.Create("sqlfiles/before/" + key + "-before.sql")
		if err != nil {
			log.Fatal(err)
		}
		fileFaild, err := os.Create("sqlfiles/faild/" + key + "-faild.sql")
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println("==================================================================")

		bf := bytes.Buffer{}
		for k, value := range data {
			var result []string
			images, err := decodeImages(value.Pictures)
			if err != nil {
				bf.WriteString(strconv.Itoa(int(value.ID)))
				bf.WriteString(", ")
				logger.Trace.Printf("carpeta: %s: decodeImages(): error %v", key, err)

				continue
			}

			if len(images) == 0 {
				bf.WriteString(strconv.Itoa(int(value.ID)))
				bf.WriteString(", ")

				continue
			}
			fmt.Printf("%s: %d/%d\n", key, len(data), k+1)

			for _, img := range images {
				if !strings.HasPrefix(img, "https://") {
					bf.WriteString(strconv.Itoa(int(value.ID)))
					bf.WriteString(", ")
					logger.Trace.Printf("carpeta: %s: !strings.HasPrefix(img, https:): error %v", key, err)

					continue
				}

				// path := strings.Split(img.Picture, "?")[0]

				res, err := http.Get(img)
				if err != nil {
					bf.WriteString(strconv.Itoa(int(value.ID)))
					bf.WriteString(", ")

					logger.Trace.Printf("carpeta: %s: http.Get(img): error %v", key, err)

					continue
				}
				bff := &bytes.Buffer{}

				_, err = io.Copy(bff, res.Body)
				if err != nil {
					bf.WriteString(strconv.Itoa(int(value.ID)))
					bf.WriteString(", ")
					logger.Trace.Printf("carpeta: %s: io.Copy: error %v", key, err)

					continue
				}

				dest := key

				if key == "community-" {
					dest = "community"
				}

				director := newService.GetDirector(filepath.Ext(img), false)
				i, err := edimage.DecodeImage(bff)
				if err != nil {
					bf.WriteString(strconv.Itoa(int(value.ID)))
					bf.WriteString(", ")
					logger.Trace.Printf("carpeta: %s: newService.GetDirector: error %v", key, err)

					continue
				}

				filename := fmt.Sprintf("%s%s", edimage.GenerateFilename(), filepath.Ext(img))

				path := fmt.Sprintf("%s/%s/%s", dest, string(edimage.Original), filename)

				originalImg, err := director.Build(i, 100)
				if err != nil {
					bf.WriteString(strconv.Itoa(int(value.ID)))
					bf.WriteString(", ")
					logger.Trace.Printf("director.Build: error %v", err)

					continue
				}

				err = storage.Upload(originalImg, path, filemanager.PublicRead)
				if err != nil {
					bf.WriteString(strconv.Itoa(int(value.ID)))
					bf.WriteString(", ")
					logger.Trace.Printf("carpeta: %s: storage.Upload: error %v", key, err)

					continue
				}

				result = append(result, filename)
				res.Body.Close()

			}

			if len(result) == 0 {
				bf.WriteString(strconv.Itoa(int(value.ID)))
				bf.WriteString(", ")
				// bf.Truncate(bf.Len() - 2)
				// queryFaild := fmt.Sprintf(query.Faild, bf.String())
				// fileFaild.Write([]byte(queryFaild))
				logger.Trace.Printf("carpeta: %s: len(result) == 0: error %v", key, err)

				continue
			}

			imgs, err := encodeImages(result)
			if err != nil {
				bf.WriteString(strconv.Itoa(int(value.ID)))
				bf.WriteString(", ")
				// bf.Truncate(bf.Len() - 2)
				// queryFaild := fmt.Sprintf(query.Faild, bf.String())
				// fileFaild.Write([]byte(queryFaild))

				logger.Trace.Printf("carpeta: %s: encodeImages: error %v", key, err)

				continue
			}

			imgsBefore, err := encodeImages(images)
			if err != nil {
				bf.WriteString(strconv.Itoa(int(value.ID)))
				bf.WriteString(", ")
				// bf.Truncate(bf.Len() - 2)
				// queryFaild := fmt.Sprintf(query.Faild, bf.String())
				// fileFaild.Write([]byte(queryFaild))
				logger.Trace.Printf("carpeta: %s: encodeImages: error %v", key, err)

				continue
			}

			queryGood := fmt.Sprintf(query.UpdateAlt, string(imgs), value.ID)
			file.Write([]byte(queryGood))

			queryBefore := fmt.Sprintf(query.UpdateAlt, string(imgsBefore), value.ID)
			fileBefore.Write([]byte(queryBefore))

		}

		if bf.Len() > 0 {
			bf.Truncate(bf.Len() - 2)
			queryFaild := fmt.Sprintf(query.Faild, bf.String())
			fileFaild.Write([]byte(queryFaild))
		}

		file.Close()
		fileBefore.Close()
		fileFaild.Close()
	}
}

func decodeImages(data json.RawMessage) ([]string, error) {
	var images []string
	err := json.Unmarshal(data, &images)
	if err != nil {
		return nil, err
	}

	return images, nil
}

func encodeImages(images []string) (json.RawMessage, error) {
	data, err := json.Marshal(images)
	if err != nil {
		return nil, err
	}

	return data, nil
}
