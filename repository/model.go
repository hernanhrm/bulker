package repository

import "encoding/json"

type Set struct {
	ID      uint
	Name    string
	Picture string
	Value   string
}

type Data struct {
	ID      uint
	Picture string
}

type DataArray struct {
	ID       uint
	Pictures json.RawMessage
}

// var photos = map[string]Table{}
type Query struct {
	Get       string
	Update    string
	UpdateAlt string
	Faild     string
	Url       string
}

var Directories = map[string]Query{
	"blogs": {
		"SELECT id, poster from blogs where poster is not null and poster != ''",
		"update blogs set poster = $1 where id = $2",
		`update blogs set poster = '%s' where id = %d;` + "\n",
		`select id, poster from blogs where id in(%s);` + "\n",
		"",
	},
	"courses": {
		"SELECT id, picture from courses where picture is not null and picture != ''",
		"update courses set picture = $1 where id = $2",
		"update courses set picture = '%s' where id = %d;" + "\n",
		`select id, picture from courses where id in(%s);` + "\n",
		"",
	},
	"specialities": {
		"SELECT id, picture from specialties where picture is not null and picture != ''",
		"update specialties set picture = $1 where id = $2",
		"update specialties set picture = '%s' where id = %d;" + "\n",
		`select id, picture from specialties where id in(%s);` + "\n",
		"",
	},
	"certificates": {
		"SELECT id, image from certificates where image is not null and image != '' order by id desc",
		"update certificates set image = $1 where id = $2",
		"update certificates set image = '%s' where id = %d;" + "\n",
		`select id, image from certificates where id in(%s);` + "\n",
		"https://api.ed.team/public-files/",
	},
	"speciality-certificates": {
		"SELECT id, image from speciality_certificates where image is not null and image != '' order by id desc",
		"update speciality_certificates set image = $1 where id = $2",
		"update speciality_certificates set image = '%s' where id = %d;" + "\n",
		`select id, image from speciality_certificates where id in(%s);` + "\n",
		"https://api.ed.team/public-files/",
	},
	"users": {
		"SELECT id, picture from users where picture is not null and picture != '' order by id desc",
		"update users set picture = $1 where id = $2",
		"update users set picture = '%s' where id = %d;" + "\n",
		`select id, picture from users where id in(%s);` + "\n",
		"https://api.ed.team/files/",
	},
	"supports": {
		"SELECT id, support from payment_supports where support is not null and support != '' order by id desc",
		"update payment_supports set support = $1 where id = $2",
		"update payment_supports set support = '%s' where id = %d;" + "\n",
		`select id, support from payment_supports where id in(%s);` + "\n",
		"https://api.ed.team/api/v1/payment-supports/admin/",
	},
}

var ArrayDirectories = map[string]Query{
	"notes": {
		"SELECT id, images from notes where images is not null order by id desc",
		"update notes set images = $1 where id = $2",
		`update notes set images = '%s' where id = %d;` + "\n",
		`select id, images from notes where id in(%s);` + "\n",
		"",
	},
	"community": {
		"SELECT id, images from publications where images is not null order by id desc",
		"update publications set images = $1 where id = $2",
		`update publications set images = '%s' where id = %d;` + "\n",
		`select id, images from publications where id in(%s);` + "\n",
		"",
	},
	"community-": {
		"SELECT id, images from publication_responses where images is not null order by id desc",
		"update publication_responses set images = $1 where id = $2",
		`update publication_responses set images = '%s' where id = %d;` + "\n",
		`select id, images from publication_responses where id in(%s);` + "\n",
		"",
	},
}
