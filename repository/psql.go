package repository

import (
	"EDteam/psql"
)

func GetAll(query string) ([]Data, error) {
	var dd []Data
	db := psql.GetConnection()

	stmt, err := db.Prepare(query)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		d := Data{}

		err := rows.Scan(&d.ID, &d.Picture)
		if err != nil {
			return nil, err
		}

		dd = append(dd, d)
	}

	return dd, nil
}

func Update(id uint, picture string, query string) error {
	db := psql.GetConnection()

	stmt, err := db.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	err = psql.ExecAffectingOneRow(
		stmt,
		picture,
		id,
	)
	if err != nil {
		return err
	}

	return nil
}

func GetAllArrays(query string) ([]DataArray, error) {
	var dd []DataArray
	db := psql.GetConnection()

	stmt, err := db.Prepare(query)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		d := DataArray{}

		err := rows.Scan(&d.ID, &d.Pictures)
		if err != nil {
			return nil, err
		}

		dd = append(dd, d)
	}

	return dd, nil
}
